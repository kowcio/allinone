# README #

Spring Tutorial (All In One)

App for learning and making code for those who do not have time nor enough energy.

### What is this repository for? ###

* For putting together all the spring 4 + Java 8 stuff that we all can not search for code but to ctrl+c and v when we need to. Also a proof of concepts for ideas / problems.

* REQUEST a tutorial sample code with functionalities in ISSUES.


* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* mvn run should do the trick

### Contribution guidelines ###

* Writing tests
* Code review
* Proposing new functionalities to be incorporated

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact