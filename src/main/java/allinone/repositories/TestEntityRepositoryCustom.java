package allinone.repositories;

import allinone.entities.TestEntity;

public interface TestEntityRepositoryCustom {
    
    
    TestEntity update(TestEntity t);
    
}
